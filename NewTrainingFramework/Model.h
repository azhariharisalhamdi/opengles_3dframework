#pragma once
#include "Shaders.h"
#include "../Utilities/utilities.h"

class Model
{
private:
	GLuint m_vboId;
	GLuint m_iboId;
	bool m_isInitialized;
	int *indicesData;
	int *verticesData;
	
	int m_nVertices = 0;

public:
	Model();
	~Model();
	bool Init(const char * filename);
	GLuint GetVBO() const;
	GLuint GetIBO() const;
	bool isInitialized();
	int m_nIndices;
};

