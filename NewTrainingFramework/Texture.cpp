#pragma once


#include "stdafx.h"
#include <stdio.h>
#include <iostream>
#include "Shaders.h"
#include "Globals.h"
#include <conio.h>
#include "Texture.h"

using namespace std;

Texture::Texture() {

}

Texture::~Texture() {

}

void Texture::Init(char *fileName) {
	glGenTextures(1, &m_textureID);
	glBindTexture(GL_TEXTURE_2D, m_textureID);
	char *imageData = LoadTGA(fileName, &iWidth, &iHeight, &bdp);
	if (bdp == 24) {
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, iWidth, iHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, imageData);
	}
	else if (bdp == 32) {
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, iWidth, iHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, imageData);
	}
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glGenerateMipmap(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, 0);

	delete imageData;
}

void Texture::Init(char *cubeFront, char *cubeBack, char *cubeTop, char *cubeButtom, char *cubeRight, char *cubeLeft)
{
	glGenTextures(1, &m_textureID);
	glBindTexture(GL_TEXTURE_CUBE_MAP, m_textureID);
	for (int i = 0; i < 6; i++)
	{
		char *imageData;
		switch (i)
		{
		case 0: imageData = LoadTGA(cubeFront, &iWidth, &iHeight, &bdp);
		case 1: imageData = LoadTGA(cubeBack, &iWidth, &iHeight, &bdp);
		case 2: imageData = LoadTGA(cubeTop, &iWidth, &iHeight, &bdp);
		case 3: imageData = LoadTGA(cubeButtom, &iWidth, &iHeight, &bdp);
		case 4: imageData = LoadTGA(cubeRight, &iWidth, &iHeight, &bdp);
		case 5: imageData = LoadTGA(cubeLeft, &iWidth, &iHeight, &bdp);
		}

		if (imageData != nullptr)
		{
			if (bdp == 24) {
				glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, iWidth, iHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, imageData);
			}
			else if (bdp == 32) {
				glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGBA, iWidth, iHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, imageData);
			}
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			glGenerateMipmap(GL_TEXTURE_CUBE_MAP);
			glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
			delete[] imageData;
		}
	}
}

int Texture::getWidth() {
	return iWidth;
}

int Texture::getHeight() {
	return iHeight;
}

int Texture::getBDP() {
	return bdp;
}

GLuint Texture::getTexture() {
	return m_textureID;
}