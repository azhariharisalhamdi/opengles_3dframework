#include <stdafx.h>
#include "Shaders.h"

int Shaders::Init( char * fileVertexShader, char * fileFragmentShader )
{
	m_vertexShader = esLoadShader( GL_VERTEX_SHADER, fileVertexShader );

	if( m_vertexShader == 0 )
	{
		return -1;
	}

	m_fragmentShader = esLoadShader( GL_FRAGMENT_SHADER, fileFragmentShader );

	if( m_fragmentShader == 0 )
	{
		glDeleteShader( m_vertexShader );
		return -2;
	}

	m_program = esLoadProgram( m_vertexShader, m_fragmentShader );

	//finding location of attributes
	m_attributes.position = glGetAttribLocation( m_program, "a_posL" );

	m_attributes.color = glGetAttribLocation(m_program, "a_col");

	m_attributes.textureAttribute = glGetAttribLocation(m_program, "a_uv");

	m_attributes.itextureAttribute = glGetUniformLocation(m_program, "u_texture");

	m_uniforms.uniTrans= glGetUniformLocation(m_program, "u_transform");

	m_uniforms.cubeVertex = glGetUniformLocation(m_program, "a_CubeVertexPos");

	m_uniforms.cubeMVP = glGetUniformLocation(m_program, "u_CubeMVPMatrix");

	m_uniforms.posTrans = glGetUniformLocation(m_program, "v_pos");

	m_uniforms.samplerCubeMap = glGetUniformLocation(m_program, "u_samplerCubeMap");
	m_uniforms.texture0 = glGetUniformLocation(m_program, "u_s_texture0");

	m_uniforms.ambiance = glGetUniformLocation(m_program, "u_ambiance");

	//finding location of uniforms
	//m_uniforms;

	return 0;
}

GLuint Shaders::GetProgram() const
{
	return m_program;
}

Shaders::Attributes Shaders::GetAttributes() const
{
	return m_attributes;
}

Shaders::Uniforms Shaders::GetUniforms() const
{
	return m_uniforms;
}

Shaders::~Shaders()
{
	glDeleteProgram( m_program );
	glDeleteShader( m_vertexShader );
	glDeleteShader( m_fragmentShader );
}