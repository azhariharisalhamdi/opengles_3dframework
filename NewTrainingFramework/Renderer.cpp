#include "stdafx.h"
#include "Renderer.h"


Renderer::Renderer()
{
}


Renderer::~Renderer()
{
}

void Renderer::doRender(Shaders *takeShaders, Matrix *takeMatrix)
{
	//Shaders *shaders = m_materials.m_takeShaders;
	Shaders *shaders = takeShaders;
	Model *model = m_materials.m_takeModels;
	Texture *texture = m_materials.m_takeTextures;
	//GLuint vbo = m_materials.takeVbo;
	//GLuint ibo = m_materials.takeIbo;

	Lighting enlightening;

	glUseProgram(shaders->GetProgram());

	glBindBuffer(GL_ARRAY_BUFFER, model->GetVBO());
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, model->GetIBO());
	glBindTexture(GL_TEXTURE_2D, texture->getTexture());

	if (shaders->GetAttributes().position != -1)
	{
		glEnableVertexAttribArray(shaders->GetAttributes().position);
		glVertexAttribPointer(shaders->GetAttributes().position, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
	}

	if (shaders->GetAttributes().color != -1)
	{
		glEnableVertexAttribArray(shaders->GetAttributes().color);
		glVertexAttribPointer(shaders->GetAttributes().color, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (char*)0 + sizeof(Vector3));
	}

	if (shaders->GetAttributes().textureAttribute != -1)
	{
		glEnableVertexAttribArray(shaders->GetAttributes().textureAttribute);
		glVertexAttribPointer(shaders->GetAttributes().textureAttribute, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (char *)0 + sizeof(Vector3) * 4);
	}

	if (shaders->GetAttributes().itextureAttribute != -1)
	{
		glUniform1i(shaders->GetAttributes().itextureAttribute, 0);
	}

	//if (shaders->GetUniforms().texture0 != -1)
	//{
	//	glUniform1i(shaders->GetUniforms().texture0, 0);
	//}

	if (shaders->GetAttributes().itextureAttribute != -1)
	{
		glUniform1i(shaders->GetAttributes().itextureAttribute, 0);
	}

	if (shaders->GetUniforms().texture0 != -1)
	{

		glUniformMatrix4fv(shaders->GetUniforms().texture0, 1, GL_FALSE, takeMatrix->m[0]);
	}

	if (shaders->GetUniforms().ambiance != -1)
	{

		glUniform4fv(shaders->GetUniforms().ambiance, 1, &enlightening.m_light.m_ambience.x);
	}

	if (shaders->GetUniforms().uniTrans != -1)
	{

		glUniformMatrix4fv(shaders->GetUniforms().uniTrans, 1, GL_FALSE, takeMatrix->m[0]);
	}

	glDrawElements(GL_TRIANGLES, model->m_nIndices, GL_UNSIGNED_INT, 0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

//	delete model;
//	delete texture;
//	delete shaders;
}
