#include "stdafx.h"
#include "Object3D.h"


Object3D::Object3D()
{
	m_transform.Scale = Vector3(1.0, 1.0, 1.0);
	m_transform.Rotation = Vector3(0.0, 0.0, 0.0);
	m_transform.Translation = Vector3(0.0, 0.0, 0.0);
}


Object3D::~Object3D()
{
}


Matrix Object3D::getWorldMatrix(Camera *camera)
{
	Matrix Scale, RotationZ, RotationY, RotationX, Translation, object;
	Scale.SetScale(m_transform.Scale);
	RotationZ.SetRotationZ(m_transform.Rotation.z);
	RotationY.SetRotationY(m_transform.Rotation.y);
	RotationX.SetRotationX(m_transform.Rotation.x);
	Translation.SetTranslation(m_transform.Translation);
	

	return (Scale * RotationZ * RotationX * RotationY * Translation) * camera->cameraView() * camera->cameraProjection();
}