#pragma once

#include "stdafx.h"
#include <stdio.h>
#include <iostream>
#include "Shaders.h"
#include "Globals.h"
#include <conio.h>

class Texture {
private:
	int iHeight, iWidth, bdp;
	GLuint m_textureID;
public:
	Texture();
	~Texture();
	void Init(char *fileName);
	void Init(char *cubeFront, char *cubeBack, char *cubeTop, char *cubeButtom, char *cubeRight, char *cubeLeft);
	int getWidth();
	int getHeight();
	int getBDP();
	GLuint getTexture();
};