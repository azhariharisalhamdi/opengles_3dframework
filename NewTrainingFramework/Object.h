#pragma once

#include "../Utilities/Math.h"
#include "Shaders.h"
#include "Globals.h"

class Object
{
	struct Transform
	{
		Vector3 Scale;
		Vector3 Rotation;
		Vector3 Translation;
		Transform()
		{
			Scale = Vector3(1.0, 1.0, 1.0);
			Rotation = Vector3(0.0, 2.0, 0.0);
			Translation = Vector3(0.0, 0.0, 0.0);
		}
	};
	Transform m_transform;

public:


	Matrix getWorldMatrix();
	Object();
	~Object();
};

