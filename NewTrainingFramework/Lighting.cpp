#include "stdafx.h"
#include "Lighting.h"


Lighting::Lighting()
{
	m_light.m_ambience = Vector4(0.5, 0.5, 0.5, 1.0);
	m_light.m_color = Vector4(1.0, 0.0, 0.0, 1.0);
	m_light.m_direction = Vector3(1.0, 0.0, 0.0);
}


Lighting::~Lighting()
{
}
