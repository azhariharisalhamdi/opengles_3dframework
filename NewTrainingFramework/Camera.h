#pragma once
#include "../Utilities/Math.h"
#include "Shaders.h"
#include "Globals.h"
#include "stdafx.h"

class Camera {
private:
	Vector3 view;
	Matrix camera_;
	Vector3 m_cameraPosition;
	Vector3 m_cameraTranslation;
	Vector3 m_cameraRotation;
	GLfloat x, y, z, param_;
	GLfloat FOVY, aspect, nearPlane, farPlane;
	Matrix m_translation, m_position;
	Matrix m_rotationX, m_rotationY, m_rotationZ;
	Matrix m_cameraworld, m_cameraview, m_cameraprojection;
	Vector4 MoveL, MoveW;
public:
	//Camera(a, b, c) : GLfloat(a), GLfloat (b), GLfloat (c);
	Camera();
	~Camera();
	//Matrix getCamera();
	void Init();
	void cameraSetCoordinate(GLfloat x, GLfloat y, GLfloat z);
	void SetTranslation(GLfloat x = 0.0, GLfloat y = 0.0, GLfloat z = 0.0);
	void SetTranslationX(GLfloat x = 0.0);
	void SetTranslationY(GLfloat y = 0.0);
	void SetTranslationZ(GLfloat z = 0.0);
	void SetRotation(GLfloat x = 0.0, GLfloat y = 0.0, GLfloat z = 0.0);
	void SetRotationX(GLfloat x = 0.0f);
	void SetRotationY(GLfloat y = 0.0f);
	void SetRotationZ(GLfloat z = 0.0f);
	void cameraSetProjection(GLfloat FOVY, GLfloat aspect, GLfloat nearPlane, GLfloat farPlane);
	void cameraSetParam(GLfloat param);
	Matrix cameraTranslation();
	Matrix cameraTranslationX();
	Matrix cameraTranslationY();
	Matrix cameraTranslationZ();
	Matrix cameraRotation();
	Matrix cameraRotationX();
	Matrix cameraRotationY();
	Matrix cameraRotationZ();
	Matrix cameraView();
	Matrix cameraProjection();
	Matrix cameraWorld();
	void moveTranslation(GLfloat x, GLfloat y, GLfloat z);
	void moveRotation(GLfloat x, GLfloat y, GLfloat z);
	Vector3 getRotation();
};