#pragma once
#include "../Utilities/Math.h"
#include "Shaders.h"
#include "Globals.h"
#include "Camera.h"

class Object3D
{
	Vector3 m_scale;
	Vector3 m_rotation;
	Vector3 m_translation;
public:
	struct Transform
	{
		Vector3 Scale;
		Vector3 Rotation;
		Vector3 Translation;

		Transform()
		{
			Scale = Vector3(1.0, 1.0, 1.0);
			Rotation = Vector3(0.0, 0.0, 0.0);
			Translation = Vector3(0.0, 0.0, 0.0);
		}
	};
	
	Transform m_transform;

	Matrix Scale;
	Matrix Rotation;
	Matrix Translation;

	Matrix getWorldMatrix(Camera *camera);
	
	Object3D();
	~Object3D();
	
};