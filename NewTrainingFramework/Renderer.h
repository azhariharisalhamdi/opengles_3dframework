#pragma once

#include "stdafx.h"
#include "../Utilities/utilities.h" // if you use STL, please include this line AFTER all other include
#include "Vertex.h"
#include "Shaders.h"
#include "Globals.h"
#include <conio.h>
#include "Math.h"
#include "iostream"

#include "Model.h"
#include "Texture.h"
#include "Camera.h"
#include "Lighting.h"

using namespace std;

class Renderer
{

public:
	Renderer();
	~Renderer();
	struct materials
	{
		Shaders *m_takeShaders;
		Model *m_takeModels;
		Texture *m_takeTextures;
		GLuint m_takeVbo;
		GLuint m_takeIbo;
	} m_materials;
	void doRender(Shaders *takeShaders, Matrix *takeMatrix);
};

