#include "stdafx.h"
#include "Model.h"
#include "Vertex.h"
#include <stdio.h>


Model::Model()
{
}


Model::~Model()
{
}


bool Model::Init(const char *filename)
{
	Vertex * verticesData;
	int * indicesData;

	int NrVertices = 0, NrIndices= 0;

	FILE * pFile;

	pFile = fopen(filename, "r");

	if (pFile != nullptr)
	{
		fscanf(pFile, "%*s %d\n", &NrVertices);
		verticesData = new Vertex[NrVertices];
		m_nIndices = NrVertices;
		for (int i=0; i < NrVertices; i++)
		{
			fscanf(pFile, "   %*d. pos:[%f, %f, %f]; norm:[%f, %f, %f]; binorm:[%f, %f, %f]; tgt:[%f, %f, %f]; uv:[%f, %f];\n", &verticesData[i].pos.x, &verticesData[i].pos.y, &verticesData[i].pos.z, &verticesData[i].norm.x, &verticesData[i].norm.y, &verticesData[i].norm.z, &verticesData[i].binorm.x, &verticesData[i].binorm.y, &verticesData[i].binorm.z, &verticesData[i].tgt.x, &verticesData[i].tgt.y, &verticesData[i].tgt.z, &verticesData[i].uv.x, &verticesData[i].uv.y);
		}
		fscanf(pFile, "%*s %d\n", &NrIndices);
		indicesData = new int[NrIndices];
		for (int i = 0; i < NrIndices; i++) {
			fscanf(pFile, "   %*d.    %d,    %d,    %d\n", &indicesData[i], &indicesData[i + 1], &indicesData[i + 2]);
			i = i + 2;
		}

		glGenBuffers(1, &m_vboId);
		glBindBuffer(GL_ARRAY_BUFFER, m_vboId);
		glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex)*NrVertices, verticesData, GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glGenBuffers(1, &m_iboId);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_iboId);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indicesData)*NrIndices, indicesData, GL_STATIC_DRAW);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	}

	

	m_nVertices = NrVertices;
	m_nIndices = NrIndices;
	m_isInitialized = true;

	return m_isInitialized;

	delete[] verticesData;
	delete[] indicesData;
}
GLuint Model::GetVBO() const
{
	return m_vboId;
}
GLuint Model::GetIBO() const
{
	return m_iboId;
}

bool Model::isInitialized()
{
	return m_isInitialized;
}