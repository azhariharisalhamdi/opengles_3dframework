#include "stdafx.h"
#include "Object.h"


Object::Object()
{
}


Object::~Object()
{
}


Matrix Object::getWorldMatrix()
{
	Matrix Scale, RotationZ, RotationY, RotationX, Translation;
	Scale.SetScale(m_transform.Scale);
	RotationZ.SetRotationZ(m_3D.m_transform.Rotation.z);
	RotationY.SetRotationY(m_3D.m_transform.Rotation.y);
	RotationX.SetRotationX(m_3D.m_transform.Rotation.x);
	Translation.SetTranslation(m_3D.m_transform.Translation);
	return Scale * RotationZ * RotationX * RotationY * Translation;
}
