#include "stdafx.h"
#include "../Utilities/utilities.h" // if you use STL, please include this line AFTER all other include
#include "Vertex.h"
#include "Shaders.h"
#include "Globals.h"
#include <conio.h>
#include "Math.h"
#include "iostream"

#include "Model.h"
#include "Texture.h"
#include "Camera.h"
#include "Renderer.h"
#include "Object3D.h"

using namespace std;

Shaders		myShaders;
Shaders		objectShaders;
Shaders		cubeShaders;

Shaders		modelShaders;
Shaders		CubeMapShaders;
Shaders		ReflectionShaders;
Shaders		BallShaders;
Shaders		LightingShaders;

GLuint vboId;
GLuint iboId;
GLuint textureId;

Model testModel;
Texture testTextureInModel;

Model testModel2;
Texture testTextureInModel2;

Model testCubeMap;

Model ballModel;

Texture lightingRed;

Texture testTextureCubeMap;

Object3D Woman;
Object3D Woman1;
Object3D CubeMap;
Object3D Ball;

Camera testCamera;

Matrix testObject;
Matrix testObject2;

Renderer ModelRenderer;
Renderer ModelRenderer1;
Renderer CubeMapRenderer;
Renderer BallRenderer;
Renderer LightingRenderer;

Vertex worldMath;
Matrix cameraView, cameraProjection;

Matrix objectTransformation;

float speed = 3.0;
float ubahZ = 0.0f;
Vector4 objecttrans;
Vector3 objectRot;
Vector4 MoveL, MoveW;

bool WIsPress = false, SIsPress = false, AIsPress = false, DIsPress = false, VK_UPIsPress = false, VK_DOWNIsPress = false, VK_RIGHTIsPress = false, VK_LEFTIsPress = false;

int Init( ESContext *esContext )
{
	glClearColor( 0.0f, 0.0f, 0.0f, 0.0f );
	glEnable(GL_DEPTH_TEST);

	testCamera.Init();

	testModel.Init("../Resources/Models/Woman1.nfg");
	testTextureInModel.Init("../Resources/Textures/Woman1.tga");

	testModel2.Init("../Resources/Models/Woman1.nfg");
	testTextureInModel2.Init("../Resources/Textures/Woman1.tga");

	testCubeMap.Init("../Resources/Models/SkyBox.nfg");
	testTextureCubeMap.Init("../Resources/Textures/Skybox_Front.tga", "../Resources/Textures/Skybox_Back.tga", "../Resources/Textures/Skybox_Top.tga", "../Resources/Textures/Skybox_Buttom.tga", "../Resources/Textures/Skybox_Right.tga", "../Resources/Textures/Skybox_Left.tga");

	ballModel.Init("../Resources/Models/Ball.nfg");
	lightingRed.Init("../Resources/Textures/Red.tga");

	ModelRenderer.m_materials.m_takeModels = &testModel;
	ModelRenderer.m_materials.m_takeTextures = &testTextureInModel;

	CubeMapRenderer.m_materials.m_takeModels = &testCubeMap;
	CubeMapRenderer.m_materials.m_takeTextures = &testTextureCubeMap;

	LightingRenderer.m_materials.m_takeModels = &ballModel;
	LightingRenderer.m_materials.m_takeTextures = &lightingRed;

	modelShaders.Init("../Resources/Shaders/ModelShader.vs", "../Resources/Shaders/ModelShader.fs");
	CubeMapShaders.Init("../Resources/Shaders/CubeMapShader.vs", "../Resources/Shaders/CubeMapShader.fs");
	ReflectionShaders.Init("../Resources/Shaders/ReflectionShader.vs", "../Resources/Shaders/ReflectionShader.fs");

	LightingShaders.Init("../Resources/Shaders/LightingShader.vs", "../Resources/Shaders/LightingShader.fs");


	return 0;
}

void Draw( ESContext *esContext )
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	cameraView = testCamera.cameraView();
	cameraProjection = testCamera.cameraProjection();

	Woman.m_transform.Scale = Vector3(0.5, 0.5, 0.5);
	Woman.m_transform.Rotation = Vector3(0.0, 0.5, 0.0);
	Woman.m_transform.Translation = Vector3(0.5, -0.4, 0.0);

	//Woman1.m_transform.Scale = Vector3(0.5, 0.5, 0.5);
	Woman1.m_transform.Translation = Vector3(-0.5, -0.4, 0.0);

	Ball.m_transform.Scale = Vector3(0.01, 0.01, 0.01);
	Ball.m_transform.Translation = Vector3(-0.5, 0.0, -0.5);

	//ModelRenderer.doRender(&modelShaders, &Woman.getWorldMatrix(&testCamera));
	//ModelRenderer.doRender(&modelShaders, &Woman1.getWorldMatrix(&testCamera));
	//CubeMapRenderer.doRender(&CubeMapShaders, &CubeMap.getWorldMatrix(&testCamera));
	
	LightingRenderer.doRender(&LightingShaders, &Ball.getWorldMatrix(&testCamera));

	eglSwapBuffers( esContext->eglDisplay, esContext->eglSurface );
}

void Update( ESContext *esContext, float deltaTime )
{
	if (WIsPress) 
	{
		MoveL = Vector4(0, 0, -deltaTime*speed, 1.0);
		MoveW = MoveL * testCamera.cameraWorld();
		testCamera.SetTranslation(MoveW.x, MoveW.y, MoveW.z);
	}
	if (SIsPress)
	{
		MoveL = Vector4(0, 0, deltaTime*speed, 1.0);
		MoveW = MoveL * testCamera.cameraWorld();
		testCamera.SetTranslation(MoveW.x, MoveW.y, MoveW.z);
	}
	if (DIsPress) {
		MoveL = Vector4(deltaTime*speed, 0, 0, 1.0);
		MoveW = MoveL * testCamera.cameraWorld();
		testCamera.SetTranslation(MoveW.x, MoveW.y, MoveW.z);
	}
	if (AIsPress)
	{
		MoveL = Vector4(-deltaTime*speed, 0, 0, 1.0);
		MoveW = MoveL * testCamera.cameraWorld();
		testCamera.SetTranslation(MoveW.x, MoveW.y, MoveW.z);
	}
	if (VK_UPIsPress){
		objectRot = testCamera.getRotation();
		objectRot.x += deltaTime*speed;
		testCamera.SetRotation(objectRot.x, objectRot.y, objectRot.z);
	}
	if (VK_DOWNIsPress)
	{
		objectRot = testCamera.getRotation();
		objectRot.x += -deltaTime*speed;
		testCamera.SetRotation(objectRot.x, objectRot.y, objectRot.z);
	}
	if (VK_RIGHTIsPress) {
		objectRot = testCamera.getRotation();
		objectRot.y += -deltaTime*speed;
		testCamera.SetRotation(objectRot.x, objectRot.y, objectRot.z);
	}
	if (VK_LEFTIsPress)
	{
		objectRot = testCamera.getRotation();
		objectRot.y += deltaTime*speed;
		testCamera.SetRotation(objectRot.x, objectRot.y, objectRot.z);
	}
}

void Key( ESContext *esContext, unsigned char key, bool bIsPressed )
{
	if (bIsPressed == true) {
		switch (key) {
		case 'W':
			WIsPress = true;
			cout << "W" << endl;
			break;
		case 'S':
			SIsPress = true;
			cout << "S" << endl;
			break;
		case 'A':
			AIsPress = true;
			cout << "A" << endl;
			break;
		case 'D':
			DIsPress = true;
			cout << "D" << endl;
			break;
		case VK_UP:
			VK_UPIsPress = true;
			break;
		case VK_DOWN:
			VK_DOWNIsPress = true;
			break;
		case VK_RIGHT:
			VK_RIGHTIsPress = true;
			break;
		case VK_LEFT:
			VK_LEFTIsPress = true;
			break;
		default:
			break;
		}
	}
	else {
		switch (key) {
		case 'W':
			WIsPress = false;
			break;
		case 'S':
			SIsPress = false;
			break;
		case 'A':
			AIsPress = false;
			break;
		case 'D':
			DIsPress = false;
			break;
		case VK_UP:
			VK_UPIsPress = false;
			break;
		case VK_DOWN:
			VK_DOWNIsPress = false;
			break;
		case VK_RIGHT:
			VK_RIGHTIsPress = false;
			break;
		case VK_LEFT:
			VK_LEFTIsPress = false;
			break;
		default:
			break;
		}
	}
}

void CleanUp()
{
	//Cleaning up the buffers
	
	//glDeleteBuffers( 1, &testModel.GetVBO());
	//glDeleteBuffers( 1, &testModel.GetIBO());
}

int _tmain( int argc, _TCHAR* argv[] )
{
	ESContext esContext;

	esInitContext( &esContext );

	esCreateWindow( &esContext, "Hello OpenGLES 2", Globals::screenWidgit clone https://azharihamdi@bitbucket.org/azhariharisalhamdi/exa_robot_gazebo_moveit_model.gitth, Globals::screenHeight, ES_WINDOW_RGB | ES_WINDOW_DEPTH );

	if( Init( &esContext ) != 0 )
		return 0;

	esRegisterDrawFunc( &esContext, Draw );
	esRegisterUpdateFunc( &esContext, Update );
	esRegisterKeyFunc( &esContext, Key );

	esMainLoop( &esContext );

	//releasing OpenGL resources
	CleanUp();

	//identifying memory leaks
	MemoryDump();
	printf( "Press any key...\n" );
	_getch();

	return 0;
}

