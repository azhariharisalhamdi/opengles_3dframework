#pragma once

#include "stdafx.h"
#include "Camera.h"
#include "Shaders.h"
#include "Globals.h"

Camera::Camera()
{

}

Camera::~Camera()
{

}

void Camera::Init() {
	m_cameraPosition.x = 0.0f;
	m_cameraPosition.y = 0.0f;
	m_cameraPosition.z = 0.0f;
	//m_cameraPosition.w = 1.0f;
	m_cameraTranslation.x = 0.0f;
	m_cameraTranslation.y = 0.0f;
	m_cameraTranslation.z = 0.0f;
	m_cameraRotation.x = 0.0f;
	m_cameraRotation.y = 0.0f;
	m_cameraRotation.z = 0.0f;
	m_cameraprojection.SetPerspective(20.0f, (float)(Globals::screenWidth / Globals::screenHeight), 0.1f, 500.0f);
}

void Camera::cameraSetCoordinate(GLfloat pointX, GLfloat pointY, GLfloat pointZ) {
	m_cameraPosition.x = pointX;
	m_cameraPosition.y = pointY;
	m_cameraPosition.z = pointZ;
}

void Camera::SetTranslation(GLfloat tpointX, GLfloat tpointY, GLfloat tpointZ) {
	m_cameraTranslation.x = tpointX;
	m_cameraTranslation.y = tpointY;
	m_cameraTranslation.z = tpointZ;
	cameraView();
}

void Camera::SetTranslationX(GLfloat tpointX){
	m_cameraTranslation.x = tpointX;
	MoveL.x = tpointX;
	MoveL.y = m_cameraTranslation.y;
	MoveL.z = m_cameraTranslation.z;
	MoveL.w = 1.0f;
	MoveW = MoveL * cameraWorld();
	SetTranslation(MoveW.x, MoveW.y, MoveW.z);
	//cameraSetCoordinate(MoveW.x, MoveW.y, MoveW.z);
	cameraView();
}

void Camera::SetTranslationY(GLfloat tpointY) {
	//m_cameraTranslation.y = tpointY;
	MoveL.x = m_cameraTranslation.x;
	MoveL.y = tpointY;
	MoveL.z = m_cameraTranslation.z;
	MoveL.w = 1.0f;
	//MoveW = MoveL * cameraWorld();
	//SetTranslation(MoveW.x, MoveW.y, MoveW.z);
	//cameraSetCoordinate(MoveW.x, MoveW.y, MoveW.z);
	//cameraView();
}

void Camera::SetTranslationZ(GLfloat tpointZ) {
	//m_cameraTranslation.z = tpointZ;
	MoveL.x = m_cameraTranslation.x;
	MoveL.y = m_cameraTranslation.y;
	MoveL.z = tpointZ;
	MoveL.w = 1.0f;
	MoveW = MoveL * cameraWorld();
	SetTranslation(MoveW.x, MoveW.y, MoveW.z);
	//cameraSetCoordinate(MoveW.x, MoveW.y, MoveW.z);
	cameraView();
}

void Camera::SetRotation(GLfloat rpointX, GLfloat rpointY, GLfloat rpointZ) {
	m_cameraRotation.x = rpointX;
	m_cameraRotation.y = rpointY;
	m_cameraRotation.z = rpointZ;

	cameraWorld();
	cameraView();
}

void Camera::SetRotationX(GLfloat rpointX) {
	m_cameraRotation.x = rpointX;
	cameraView();
	cameraWorld();
}
void Camera::SetRotationY(GLfloat rpointY) {
	m_cameraRotation.y = rpointY;
	cameraView();
	cameraWorld();
}
void Camera::SetRotationZ(GLfloat rpointZ) {
	m_cameraRotation.z = rpointZ;
	cameraView();
	cameraWorld();
}

void Camera::cameraSetParam(GLfloat param) {
	this->param_ = param;
}


Matrix Camera::cameraTranslation() {
	m_translation.SetTranslation(this->m_cameraTranslation.x, this->m_cameraTranslation.y, this->m_cameraTranslation.z);
	return m_translation;
	cameraView();
	cameraWorld();
}

Matrix Camera::cameraRotationX() {
	m_rotationX.SetRotationX(this->m_cameraRotation.x);
	return m_rotationX;
}

Matrix Camera::cameraRotationY() {
	m_rotationY.SetRotationY(this->m_cameraRotation.y);
	return m_rotationY;
}

Matrix Camera::cameraRotationZ() {
	m_rotationZ.SetRotationZ(this->m_cameraRotation.z);
	return m_rotationZ;
}

Matrix Camera::cameraRotation() {
	return (m_rotationX * m_rotationY * m_rotationZ);
}

Matrix Camera::cameraView() {
	m_rotationX.SetRotationX(-m_cameraRotation.x);
	m_rotationY.SetRotationY(-m_cameraRotation.y);
	m_translation.SetTranslation(-m_cameraTranslation.x, -m_cameraTranslation.y, -m_cameraTranslation.z);
	m_cameraview = m_translation * m_rotationY * m_rotationX;
	return m_cameraview;
}
Matrix Camera::cameraProjection() {
	return this->m_cameraprojection;
}

Matrix Camera::cameraWorld() {
	//m_cameraworld = -m_cameraRotation.y * -m_cameraRotation.x * -m_cameraPosition;
	m_rotationX.SetRotationX(m_cameraRotation.x);
	m_rotationY.SetRotationY(m_cameraRotation.y);
	m_translation.SetTranslation(m_cameraTranslation.x, m_cameraTranslation.y, m_cameraTranslation.z);
	m_cameraworld = m_rotationX * m_rotationY * m_translation;
	return m_cameraworld;
}

void Camera::moveTranslation(GLfloat x, GLfloat y, GLfloat z) {
	this->m_cameraTranslation.x = x;
	this->m_cameraTranslation.y = y;
	this->m_cameraTranslation.z = z;
	//this->m_cameraTranslation.x = x;
	cameraView();
	cameraWorld();
}
void Camera::moveRotation(GLfloat x, GLfloat y, GLfloat z) {
	this->m_cameraRotation.x = x;
	this->m_cameraRotation.y = y;
	this->m_cameraRotation.z = z;
	cameraView();
	cameraWorld();
}

Vector3 Camera::getRotation() {
	Vector3 temp;
	temp.x = m_cameraRotation.x;
	temp.y = m_cameraRotation.y;
	temp.z = m_cameraRotation.z;

	return temp;
}
