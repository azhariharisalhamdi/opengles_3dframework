#pragma once
#include "../Utilities/Math.h"
#include "Shaders.h"
#include "Globals.h"
#include "Camera.h"


class Lighting
{
public:
	struct light
	{
		Vector4 m_ambience;
		Vector4 m_color;
		Vector3 m_direction;
	};

	light m_light;
	Lighting();
	~Lighting();
};

