precision mediump float;
varying vec2 v_uv;
uniform sampler2D u_s_texture0;
uniform vec4 u_ambiance;

void main()
{	
	//u_ambiance = (0.5, 0.5, 0.5, 1.0);
	vec4 ObjColor = texture2D(u_s_texture0, v_uv);
	gl_FragColor = vec4((ObjColor*u_ambiance).rgb, ObjColor.a);	
}