attribute vec3 a_posL;
attribute vec4 a_col;

varying vec4 v_col;
attribute vec2 a_uv;
varying vec2 v_uv;

uniform mat4 u_transform;

void main()
{
	v_col = a_col;
	vec4 posL = vec4(a_posL, 1.0);
	gl_Position = u_transform * posL;
	v_uv  = a_uv;
}
   